// @ts-ignore
import NodeEnvironment from 'jest-environment-node';
import { SeleniumBrowser, setup } from 'selenium-testing';

class SeleniumWebdriverEnvironment extends NodeEnvironment {
  // @ts-ignore Set by super
  public global: { [key: string]: any };
  private browsers: SeleniumBrowser[] = [];

  constructor(config: jest.ProjectConfig) {
    super(config);

    for (const browser of setup(config.testEnvironmentOptions)) {
      this.browsers.push(browser);
    }
  }

  public async setup() {
    await super.setup();

    // Expose browser drivers to use in the test files
    this.global.browsers = this.browsers;
  }

  public async teardown() {
    // Quit each browser driver
    for (const browser of this.browsers) {
      await browser.stop();
    }

    await super.teardown();
  }
}

export = SeleniumWebdriverEnvironment;
