# Jest Test Environment - Selenium WebDriver

Jest environment for running Selenium WebDriver tests.

## Development

Install peer dependencies with:

```bash
npx npm-install-peer
```